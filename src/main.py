import hashlib
from colorama import Fore, Style
import os
import csv


def getHashes(rootDir, hasher, hashes, depth):
    for filename in os.listdir(rootDir):
        filePath = os.path.join(rootDir, filename)
        try:
            if os.path.isfile(filePath):
                with open(filePath, "rb") as f:
                    buf = f.read()
                    hasher.update(buf)
                    hashes[filePath] = {
                        "File Hash": hasher.hexdigest(),
                        "File Size": os.path.getsize(filePath),
                        "Date Modified": os.path.getmtime(filePath),
                        "Date Created": os.path.getctime(filePath)
                    }
            else:
                if depth > 0:
                    getHashes(filePath, hasher, hashes, depth - 1)
        except:
            pass


def writeToCSV(hashes, HEADER):
    homeDir = os.path.expanduser('~')
    dataDir = os.path.join(homeDir, ".Sentinel")
    dataFile = os.path.join(dataDir, "sentinel_data.csv")
    if not os.path.exists(dataDir):
        os.makedirs(dataDir)

    with open(dataFile, "w", newline="", encoding="utf-8") as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(HEADER)
        for key in hashes.keys():
            valueDict = hashes[key]
            row = [key]
            for subValue in valueDict.values():
                row.append(subValue)
            writer.writerow(row)


def readCSV():
    homeDir = os.path.expanduser('~')
    dataFile = os.path.join(homeDir, ".Sentinel", "sentinel_data.csv")

    oldHashes = {}

    if os.path.isfile(dataFile):
        with open(dataFile, "r+", newline="", encoding="utf-8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=",")
            firstLine = True
            HEADER = []
            for row in csv_reader:
                if firstLine:
                    HEADER = row
                    firstLine = False
                else:
                    oldHashes[row[0]] = {
                        HEADER[1]: row[1],
                        HEADER[2]: row[2],
                        HEADER[3]: row[3],
                        HEADER[4]: row[4]
                    }

    return oldHashes


# for key in hashes:
#     print("------------------------------------------------------------------")
#     value = hashes[key]
#     printColorStart = f"{Fore.WHITE}"
#     printColorEnd = f"{Style.RESET_ALL}"
#     if value["Created"] != value["Modified"]:
#         printColorStart = f"{Fore.RED}"
#         printColorEnd = f"{Style.RESET_ALL}"
#         # print(printColorStart, "Creation and Modified Date Same", printColorEnd)
#         # print("Creation and Modified Date Same")
#     print(printColorStart, key, printColorEnd)
#     for subKey in value.keys():
#         subValue = value[subKey]
#         # print(subKey, ":", printColorStart, subValue, printColorEnd)
#     print("------------------------------------------------------------------")


# Save all data as log files
# Print if creation=modified and if file was edited

homeDir = os.path.expanduser('~')
hasher = hashlib.sha1()
hashes = {}

HEADER = ["File Path", "File Hash", "File Size",
          "Date Modified", "Date Created"]

getHashes(homeDir, hasher, hashes, 1)

# writeToCSV(hashes, HEADER)

oldHashes = readCSV()

# for key in oldHashes:
#     print(key)
#     for subKey in oldHashes[key].keys():
#         print(subKey, oldHashes[key][subKey])

# for key in oldHashes.keys():
#     fileStats = oldHashes[key]
#     for stat in fileStats:
#         if stat in hashes[key].keys():
#             if hashes[key][stat] != fileStats[stat]:
#                 print("=====================================\n", "File Edited",  key,
#                       fileStats[stat], hashes[key][stat], "\n=====================================\n")

# targetPath = os.path.join(homeDir, ".Albatross", "albatross_data.csv")
# print(oldHashes[targetPath]["File Hash"])
# print(hashes[targetPath]["File Hash"])

targetPath = os.path.join(homeDir, "Documents", "test.txt")
print(oldHashes[targetPath]["File Hash"])
print(hashes[targetPath]["File Hash"])
